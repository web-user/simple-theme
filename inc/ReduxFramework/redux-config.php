<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }


    // This is your option name where all the Redux data is stored.
    $opt_name = "comfort_options";


    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        //'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Редактировать контент', 'comfort' ),
        'page_title'           => __( 'Редактировать контент', 'comfort' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => ' AIzaSyD2zNZbGjGwA2JSPWuVF3A_GpwXqGefJdA',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-admin-generic',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => false,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => false,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => 60,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.
        'show_options_object' => false,

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );


// $my_args = array(
//     'taxonomy' => 'category',
//     'hide_empty' => false,
// );
// $terms = get_terms( $my_args );

// if (!empty($terms)) {
//     foreach ( $terms as $term) {
//         $my_cat[$term->term_id] = $term->name;
//         $my_val[$term->term_id] = 0;
//     }
// }

Redux::setArgs( $opt_name, $args );



// BE SURE TO RENAME THE FUNCTION NAMES TO YOUR OWN NAME OR PREFIX
 if ( !function_exists( "df_redux_add_metaboxes" ) ):
     $redux_opt_name = 'comfort_options';
     function df_redux_add_metaboxes($metaboxes) {
         // Declare your sections

         print_r('MMMOO----');



         $boxSections = array();
         $boxSections[] = array(
             'title'         => __('General Settings', 'redux-framework-demo'),
//             'icon'          => 'el-icon-home', // Only used with metabox position normal or advanced
             'fields'        => array(
                 array(
                     'id' => 'sidebar-custom',
                     'title' => __( 'Sidebar', 'redux-framework-demo' ),
                     'desc' => 'Please select the sidebar you would like to display on this page. Note: You must first create the sidebar under Appearance > Widgets.',
                     'type' => 'repeater',
                     'limit'      => 3,
                     'fields'     => array(
                         array(
                             'id'          => 'phone_number_f',
                             'title'       => 'Номер телефона или другой контакт',
                             'type'        => 'text',
                             'placeholder' => __( '', 'comfort' ),
                         ),
                         array(
                             'id'          => 'phone_type_h',
                             'type'        => 'select',
                             'title' => __( 'Тип контакта', 'comfort' ),
                             'options'     => array(
                                 'phone'             => __( 'Телефон', 'comfort' ),
                                 'skype'             => __( 'Skype', 'comfort' ),
                                 'viber'             => __( 'Viber', 'comfort' ),
                                 'email'             => __( 'Email', 'comfort' ),

                             ),
                             'placeholder' => __( 'Выберите тип', 'comfort' ),
                         ),

                     )
                 ),
             ),
         );


         $boxSections[] =  array(
             'icon' => 'el-icon-screen',
             'fields' => array(
                 array(
                     'id'        => 'post-gallery-custom',
                     'type'      => 'slides',
                     'title'     => esc_html__('Gallery Slider', 'magplus'),
                     'subtitle'  => esc_html__('Upload images or add from media library.', 'magplus'),
                     'placeholder'   => array(
                         'title'         => esc_html__('Title', 'magplus'),
                     ),
                     'show' => array(
                         'title'       => true,
                         'description' => true,
                         'url'         => true,
                     )
                 ),
             )
         );
 
         // Declare your metaboxes
         $metaboxes = array();
         $metaboxes[] = array(
             'id'            => 'sidebar-custom',
             'title'         => __( 'Sidebar', 'fusion-framework' ),
             'post_types'    => array( 'page', 'post' ),
//             'page_template' => array('page-test.php'), // Visibility of box based on page template selector
             //'post_format' => array('image'), // Visibility of box based on post format
             'position'      => 'normal', // normal, advanced, side
             'priority'      => 'high', // high, core, default, low - Priorities of placement
             'sections'      => $boxSections,
         );
 
         return $metaboxes;
     }
     // Change {$redux_opt_name} to your opt_name
     print_r($opt_name);
     add_action("redux/metaboxes/{$opt_name}/boxes", "df_redux_add_metaboxes");
 endif;
// 
// * * ---> END ARGUMENTS */
    









    /*
     * ---> SECTIONS
     */    
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Шапка сайта', 'comfort' ),
        'id'               => 'header',
        'desc'             => __( '', 'comfort' ),
        'customizer_width' => '400px',
        'icon'             => 'el el-chevron-up',
        'fields'           => array(
            array(
                'id'       => 'switch_admin_bar',
                'type'     => 'switch',
                'title'    => 'Панель администратора',
                'subtitle' => 'Включить/выключить панель администратора.',
                'default'  => true,
            ),
            array(
                'id'       => 'logo_header',
                'type'     => 'media',
                'title'    => __('Логотип', 'comfort'),
                'desc'     => __('', 'comfort'),
                'subtitle' => __('Размер логотипа <code>241 × 86 px.</code>', 'comfort'),
                'default'  => array('url'=>'http://comfort.loc/wp-content/uploads/2018/06/logo_header.png'),
            ),
            array(
                'id'       => 'request_to_measure',
                'type'     => 'switch',
                'title'    => 'Кнопка заявки на замер',
                'subtitle' => 'Включить/выключить кнопку в шапке сайта.',
                'default'  => true,
            ),
            array(
                'id'       => 'request_to_measure_name',
                'type'     => 'text',
                'title'    => __( '', 'comfort' ),
                'subtitle' => __( 'Текст кнопки', 'comfort' ),
                'required' => array( 'request_to_measure', '=', true ),
                'default'  => 'Оставить заявку на замер',
            ),
            array(
                'id'       => 'request_to_recall',
                'type'     => 'switch',
                'title'    => 'Кнопка обратного звонка',
                'subtitle' => 'Включить/выключить кнопку в шапке сайта.',
                'default'  => true,
            ),
            array(
                'id'       => 'request_to_recall_name',
                'type'     => 'text',
                'title'    => __( '', 'comfort' ),
                'subtitle' => __( 'Текст кнопки', 'comfort' ),
                'required' => array( 'request_to_recall', '=', true ),
                'default'  => 'Обратный звонок',
            ),
            array(
                'id'         => 'phones_header',
                'type'       => 'repeater',
                'title'      => __( 'Контакты в шапке', 'comfort' ),
                'subtitle'   => __( 'Порядок контактов можно менять перетаскивая их', 'comfort' ),
                'desc'       => __( '', 'comfort' ),
                'limit'      => 3,
                'sortable' => true,
                'group_values' => true,
                'fields'     => array(
                    array(
                        'id'          => 'phone_number',
                        'title'       => 'Номер телефона или другой контакт',
                        'type'        => 'text',
                        'placeholder' => __( '', 'comfort' ),
                    ),
                    array(
                        'id'          => 'phone_type',
                        'type'        => 'select',
                        'title' => __( 'Тип контакта', 'comfort' ),
                        'options'     => array(
                            'phone'             => __( 'Телефон', 'comfort' ),
                            'skype'             => __( 'Skype', 'comfort' ),
                            'viber'             => __( 'Viber', 'comfort' ),
                            'email'             => __( 'Email', 'comfort' ),

                        ),
                        'placeholder' => __( 'Выберите тип', 'comfort' ),
                    ),
                    array(
                        'id'       => 'phone_icon',
                        'type'     => 'media',
                        'title'    => __('Иконка', 'comfort'),
                        'desc'     => __('', 'comfort'),
                        'subtitle' => __('Размер иконки <code>24 × 24 px.</code>', 'comfort'),
                        'default'  => array('url'=>''),
                    ),
                )
            ),
            array(
                'id'      => 'timetable',
                'type'    => 'editor',
                'title'   => __( 'Режим работы', 'comfort' ),
                'default' => 'Пн - Вс с 08:00 до 20:00 <br>Без выходных.',
                'args'    => array(
                    'wpautop'       => false,
                    'media_buttons' => false,
                    'textarea_rows' => 2,
                    'teeny'         => false,
                    'quicktags'     => false,
                )
            ),
        )
    ) );




    if ( class_exists( 'RevSlider' ) ) {
        $rev_slider = new RevSlider();
        $sliders = $rev_slider->getAllSliderAliases();
    } else {
        $sliders = array();
    }

    Redux::setSection( $opt_name, array(
        'title'            => __( 'Контент страниц', 'comfort' ),
        'id'               => 'page',
        'desc'             => __( '', 'comfort' ),
        'customizer_width' => '400px',
        'icon'             => 'el el-align-justify',
        'fields'           => array(
            array(
                'id'       => 'switch_service_menu',
                'type'     => 'switch',
                'title'    => 'Сервисное меню',
                'subtitle' => 'Включить/выключить сервисное меню.',
                'default'  => true,
            ),
            array(
                'id'       => 'switch_rev_slider',
                'type'     => 'switch',
                'title'    => 'Слайдер на главной странице',
                'subtitle' => 'Включить/выключить слайдер на главной странице.',
                'default'  => true,
            ),
            array(
                'id'       => 'switch_rev_slider_name',
                'type'     => 'select',
                'title'    => __( '', 'comfort' ),
                'subtitle' => __( 'Выбрать слайдер', 'comfort' ),
                'desc'     => __( '', 'comfort' ),
                'required' => array( 'switch_rev_slider', '=', true ),
                'options'  => $sliders,
                'default'  => 0,
            ),
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'            => __( 'Подвал сайта', 'comfort' ),
        'id'               => 'footer',
        'desc'             => __( '', 'comfort' ),
        'customizer_width' => '400px',
        'icon'             => 'el el-chevron-down',
        'fields'           => array(
            array(
                'id'       => 'logo_footer',
                'type'     => 'media',
                'title'    => __('Логотип в подвале', 'comfort'),
                'desc'     => __('', 'comfort'),
                'subtitle' => __('Размер логотипа <code>219 × 78 px.</code>', 'comfort'),
                'default'  => array('url'=>'http://comfort.loc/wp-content/uploads/2018/06/logo_footer.png'),
            ),
            array(
                'id'      => 'adress',
                'type'    => 'editor',
                'title'   => __( 'Адресс', 'comfort' ),
                'default' => '40 лет Советской Украины 47а, Запорожье, Запорожская область, 69000.',
                'args'    => array( 
                    'wpautop'       => false,
                    'media_buttons' => false,
                    'textarea_rows' => 2,
                    'teeny'         => false,
                    'quicktags'     => false,
                )
            ),
            array(
                'id'       => 'adress_icon',
                'type'     => 'media',
                'title'    => __('Иконка для адреса', 'comfort'),
                'desc'     => __('', 'comfort'),
                'subtitle' => __('Размер иконки <code>24 × 24 px.</code>', 'comfort'),
                'default'  => array('url'=>'http://comfort.loc/wp-content/uploads/2018/06/point.png'),
            ),
            array(
                'id'         => 'phones_footer',
                'type'       => 'repeater',
                'title'      => __( 'Контакты в подвале', 'comfort' ),
                'subtitle'   => __( 'Порядок контактов можно менять перетаскивая их', 'comfort' ),
                'desc'       => __( '', 'comfort' ),
                'limit'      => 3,
                'sortable' => true,
                'group_values' => true,
                'fields'     => array(
                    array(
                        'id'          => 'phone_number',
                        'title'       => 'Номер телефона или другой контакт',
                        'type'        => 'text',
                        'placeholder' => __( '', 'comfort' ),
                    ),
                    array(
                        'id'          => 'phone_type',
                        'type'        => 'select',
                        'title' => __( 'Тип контакта', 'comfort' ),
                        'options'     => array(
                            'phone'             => __( 'Телефон', 'comfort' ),
                            'skype'             => __( 'Skype', 'comfort' ),
                            'viber'             => __( 'Viber', 'comfort' ),
                            'email'             => __( 'Email', 'comfort' ),

                        ),
                        'placeholder' => __( 'Выберите тип', 'comfort' ),
                    ),
                    array(
                        'id'       => 'phone_icon',
                        'type'     => 'media',
                        'title'    => __('Иконка', 'comfort'),
                        'desc'     => __('', 'comfort'),
                        'subtitle' => __('Размер иконки <code>24 × 24 px.</code>', 'comfort'),
                        'default'  => array('url'=>''),
                    ),
                )
            ),
            array(
                'id'         => 'social_footer',
                'type'       => 'repeater',
                'title'      => __( 'Социальные иконки в подвале', 'comfort' ),
                'subtitle'   => __( 'Порядок иконок можно менять перетаскивая их', 'comfort' ),
                'desc'       => __( '', 'comfort' ),
                'limit'      => 3,
                'sortable' => true,
                'group_values' => true,
                'fields'     => array(
                    array(
                        'id'          => 'social_link',
                        'title'       => 'Ссылка на социальную сеть',
                        'type'        => 'text',
                        'placeholder' => __( '', 'comfort' ),
                    ),
                    array(
                        'id'       => 'social_icon',
                        'type'     => 'media',
                        'title'    => __('Иконка социальной сети', 'comfort'),
                        'desc'     => __('', 'comfort'),
                        'subtitle' => __('Размер иконки <code>29 × 29 px.</code>', 'comfort'),
                        'default'  => array('url'=>''),
                    ),
                )
            ),
            array(
                'id'       => 'download_price',
                'type'     => 'switch',
                'title'    => 'Кнопка скачать прайс',
                'subtitle' => 'Включить/выключить кнопку в подвале сайта.',
                'default'  => true,
            ),
            array(
                'id'       => 'download_price_name',
                'type'     => 'text',
                'title'    => __( '', 'comfort' ),
                'subtitle' => __( 'Текст кнопки', 'comfort' ),
                'required' => array( 'download_price', '=', true ),
                'default'  => 'Посмотреть прайс',
            ),
            array(
                'id'       => 'download_price_link',
                'type'     => 'media',
                'title'    => __('Ссылка на прайс', 'comfort'),
                'desc'     => __('', 'comfort'),
                'subtitle' => __('', 'comfort'),
                'mode'     => false,
                'required' => array( 'download_price', '=', true ),
                'default'  => array('url'=>''),
            ),
            array(
                'id'       => 'copyright',
                'type'     => 'text',
                'title'    => __( 'Текст про права', 'comfort' ),
                'desc'     => __('<code>%year%</code> будет заменен на текущий год.', 'comfort'),
                'subtitle' => __( '', 'comfort' ),
                'default'  => '%year% COMFORT_ZP',
            ),

        )
    ) );

    // Redux::setSection( $opt_name, array(
    //     'title'            => __( 'Категории сервисов', 'comfort' ),
    //     'id'               => 'services',
    //     'desc'             => __( 'Здесь можно редактировать иконки сервисов', 'comfort' ),
    //     'customizer_width' => '400px',
    //     'icon'             => 'el el-tags',
    //     'fields'           => array(
    //         // array(
    //         //     'id'       => 'test-field',
    //         //     'type'     => 'testfield',
    //         //     'title'    => __( 'Testfield', 'comfort' ),
    //         //     'subtitle' => __( '', 'comfort' ),
    //         //     'default'  => false
    //         // ),
    //         array(
    //             'id'       => 'cat-windows',//opt-required-basic
    //             'type'     => 'switch',
    //             'title'    => __( 'Окна', 'comfort' ),
    //             'subtitle' => __( '', 'comfort' ),
    //             'default'  => false
    //         ),
    //         array(
    //             'id'       => 'cat-windows-name',
    //             'type'     => 'text',
    //             'title'    => __( 'Название категории', 'comfort' ),
    //             'subtitle' => __( '', 'comfort' ),
    //             'required' => array( 'cat-windows', '=', true )
    //         ),
    //         array(
    //             'id'       => 'cat-windows-pic',
    //             'type'     => 'media',
    //             'title'    => __('Иконка', 'comfort'),
    //             'desc'     => __('', 'comfort'),
    //             'subtitle' => __('', 'comfort'),
    //             'required' => array( 'cat-windows', '=', true )
    //         ),

    //         array(
    //             'id'      => 'wpml-multicheck',
    //             'type'    => 'checkbox',
    //             'title'   => __( 'Checkbox Taxonomy', 'comfort' ),
    //             'desc'    => __( '', 'comfort' ),
    //             'type'     => 'sortable',
    //             'mode'     => 'checkbox', // checkbox or text
    //             //Must provide key => value pairs for multi checkbox options
    //             'options' => $my_cat,
    //             'default' => $my_val,
    //         ),
    //         array(
    //             'id'       => 'opt-required-nested',
    //             'type'     => 'switch',
    //             'title'    => 'Nested Required Example',
    //             'subtitle' => 'Click <code>On</code> to see another set of options appear.',
    //             'default'  => false
    //         ),
    //         array(
    //             'id'       => 'opt-required-nested-buttonset',
    //             'type'     => 'button_set',
    //             'title'    => 'Multiple Nested Required Examples',
    //             'subtitle' => 'Click any buton to show different fields based on their <code>required</code> statements.',
    //             'options'  => array(
    //                 'button-text'     => 'Show Text Field',
    //                 'button-textarea' => 'Show Textarea Field',
    //                 'button-editor'   => 'Show WP Editor',
    //                 'button-ace'      => 'Show ACE Editor'
    //             ),
    //             'required' => array( 'opt-required-nested', '=', true ),
    //             'default'  => 'button-text'
    //         ),
    //         array(
    //             'id'       => 'opt-required-nested-text',
    //             'type'     => 'text',
    //             'title'    => 'Nested Text Field',
    //             'required' => array( 'opt-required-nested-buttonset', '=', 'button-text' )
    //         ),
    //         array(
    //             'id'       => 'opt-required-nested-textarea',
    //             'type'     => 'textarea',
    //             'title'    => 'Nested Textarea Field',
    //             'required' => array( 'opt-required-nested-buttonset', '=', 'button-textarea' )
    //         ),
    //         array(
    //             'id'       => 'opt-required-nested-editor',
    //             'type'     => 'editor',
    //             'title'    => 'Nested Editor Field',
    //             'required' => array( 'opt-required-nested-buttonset', '=', 'button-editor' )
    //         ),
    //         array(
    //             'id'       => 'opt-required-nested-ace',
    //             'type'     => 'ace_editor',
    //             'title'    => 'Nested ACE Editor Field',
    //             'required' => array( 'opt-required-nested-buttonset', '=', 'button-ace' )
    //         ),
    //         array(
    //             'id'       => 'opt-required-select',
    //             'type'     => 'select',
    //             'title'    => 'Select Required Example',
    //             'subtitle' => 'Select a different option to display its value.  Required may be used to display multiple & reusable fields',
    //             'options'  => array(
    //                 'no-sidebar'    => 'No Sidebars',
    //                 'left-sidebar'  => 'Left Sidebar',
    //                 'right-sidebar' => 'Right Sidebar',
    //                 'both-sidebars' => 'Both Sidebars'
    //             ),
    //             'default'  => 'no-sidebar',
    //             'select2'  => array( 'allowClear' => false )
    //         ),
    //         array(
    //             'id'       => 'opt-required-select-left-sidebar',
    //             'type'     => 'select',
    //             'title'    => 'Select Left Sidebar',
    //             'data'     => 'sidebars',
    //             'default'  => '',
    //             'required' => array( 'opt-required-select', '=', array( 'left-sidebar', 'both-sidebars' ) )
    //         ),
    //         array(
    //             'id'       => 'opt-required-select-right-sidebar',
    //             'type'     => 'select',
    //             'title'    => 'Select Right Sidebar',
    //             'data'     => 'sidebars',
    //             'default'  => '',
    //             'required' => array( 'opt-required-select', '=', array( 'right-sidebar', 'both-sidebars' ) )
    //         ),
            // array(
            //     'id'       => 'aa_dwn1_cat',
            //     'type'     => 'select',
            //     'data'     => 'categories',
            //     'args'     => array('taxonomy' => array('category'), 'hide_empty' => false),
            //     'title'    => __( 'Select Category', 'comfort' ),
            //     'subtitle' => __( '', 'comfort' ),
            // ),
    //     )
    // ) );

    // Redux::setSection( $opt_name, array(
    //     'title'            => __( 'Связаться с нами', 'comfort' ),
    //     'id'               => 'contacts',
    //     'desc'             => __( 'Здесь можно редактировать все кнопки связи с нами', 'comfort' ),
    //     'customizer_width' => '400px',
    //     'icon'             => 'el el-bell',
    //     'fields'           => array(

    //     )
    // ) );

    // Redux::setSection( $opt_name, array(
    //     'title'            => __( 'Слайдер', 'comfort' ),
    //     'id'               => 'header-testbasic',
    //     'desc'             => __( 'Здесь можно редактировать слайдер', 'comfort' ),
    //     'customizer_width' => '400px',
    //     'icon'             => 'el el-picture',
    //     'fields'           => array(
    //         array(
    //             'id'          => 'slides',
    //             'type'        => 'slides',
    //             // 'title'       => __( '', 'comfort' ),
    //             // 'subtitle'    => __( '', 'comfort' ),
    //             // 'desc'        => __( '', 'comfort' ),
    //             'placeholder' => array(
    //                 'title'       => __( 'Заголовок', 'comfort' ),
    //                 'description' => __( 'Текст слайдера', 'comfort' ),
    //                 'url'         => __( 'Ссылка', 'comfort' ),
    //             ),
    //         ),
    //     )
    // ) );